import pandas as pd
import numpy as np
import sys
import os
import pyspark
from pyspark.sql import SparkSession
from pyspark.sql import DataFrameReader
from pyspark.sql import SQLContext
from pyspark import SparkContext
import re
import geopandas as gpd
import matplotlib.pyplot as plt
import seaborn as sns
import warnings
from pyspark.sql.functions import col, format_string, date_format, format_number
from pyspark.sql.functions import count, sum, avg, expr
from pyspark.sql import functions as F
warnings.filterwarnings('ignore')

from google.cloud import storage
from pyspark.sql import SparkSession

import sys
from pathlib import Path

# Dodaj folder nadrzędny dla 'app' do sys.path
parent_dir = str(Path(__file__).resolve().parent.parent)
if parent_dir not in sys.path:
    sys.path.append(parent_dir)

# Teraz możesz importować z 'app'
# from app import [inny_moduł]


from app.jobs import flights_job
from app.jobs.aggregates import functions


spark = SparkSession.builder \
        .appName('Airports_aggregates_per_day') \
        .config('spark.jars', 'gs://dataproc-staging-europe-west1-305196457839-tm6piscn/notebooks/jupyter/FLIGHTS/app/bigquery_connection/spark-3.1- bigquery-0.28.0-preview.jar') \
        .getOrCreate()


def main():

    run_job(spark = spark)


if __name__ == '__main__':
    main()