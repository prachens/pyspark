from app.jobs.aggregates.functions import get_df, create_main_dataframe, num_of_flights_aggregate
from app.jobs.aggregates.functions import avg_delay_time, avg_flight_occupancy, avg_occupancy_class
from app.jobs.aggregates.functions import avg_passengers_served,avg_flights_nb

spark_session = SparkSession.builder \
        .appName('Airports_aggregates_per_day') \
        .config('spark.jars', 'gs://dataproc-staging-europe-west1-305196457839-tm6piscn/notebooks/jupyter/FLIGHTS/app/bigquery_connection/spark-3.1- bigquery-0.28.0-preview.jar') \
        .getOrCreate()

def _extract_data(spark):
    df_aggregates = get_df(table_name='aggregates', spark = spark_session)
    df_aircrafts_data = get_df(table_name='aircrafts_data', spark = spark_session)
    df_airports_coordinates = get_df(table_name='airports_coordinates', spark = spark_session)
    df_airports_data = get_df(table_name='airports_data', spark = spark_session)
    df_boarding_passes = get_df(table_name='boarding_passes', spark = spark_session)
    df_bookings = get_df(table_name='bookings', spark = spark_session)
    df_flights = get_df(table_name='flights', spark = spark_session)
    df_seats = get_df(table_name='seats', spark = spark_session)
    df_ticket_flights = get_df(table_name='ticket_flights', spark = spark_session)
    df_ticket_flights_v1 = get_df(table_name='ticket_flights_v1', spark = spark_session)
    df_tickets = get_df(table_name='tickets', spark = spark_session)
    
    # creating aliases
    bookings = df_bookings.alias('b')
    tickets = df_tickets.alias('t')
    ticket_flights = df_ticket_flights.alias('tf')
    flights = df_flights.alias('f')
    aircrafts_data = df_aircrafts_data.alias('ad')
    seats = df_seats.alias('s')
    
    
    df = create_main_dataframe(spark_session, bookings, tickets, ticket_flights, flights)
    
    return df

def _tranform_data(spark):
    num_of_flights_df = num_of_flights_aggregate(
        spark_session, 
        df
    )
    avg_delay_time_df = avg_delay_time(
        spark_session,
        df
    )
    avg_flight_occupancy_df = avg_flight_occupancy(
        spark_session,
        df
    )
    avg_occupancy_class_df = avg_occupancy_class(
        spark_session,
        df
    )
    avg_passengers_served_df = avg_passengers_served(
        spark_session,
        df
    )
    avg_flights_nb_df = avg_flights_nb(
        spark_session,
        df
    )
    return num_of_flights_df, avg_delay_time_df, avg_flight_occupancy_df, avg_occupancy_class_df, avg_passengers_served_df, avg_flights_nb_df
    

def _load_data(spark, df):
    # zapisanie finalnej tabelki do google cloud
    tabelka_finalna.write.format("com.google.cloud.spark.bigquery") \
                    .option("writeMethod", "direct") \
                    .option("temporaryGcsBucket", 'dataproc-staging-europe-central2-305196457839-jazljgsi') \
                    .option("allowUnknownTypes", "true") \
                    .save(f'flights_database.{df}')

def run_job(spark):
    df = _extract_data(spark)
    
    num_of_flights, avg_delay_time, 
    avg_flight_occupancy, avg_occupancy_class, 
    avg_passengers_served, avg_flights_nb = _tranform_data(spark)
    
    _load_data(spark, df)