from pyspark.sql import SparkSession


from google.cloud import storage
from pyspark.sql import SparkSession

import sys
from pathlib import Path

# Dodaj folder nadrzędny dla 'app' do sys.path
parent_dir = str(Path(__file__).resolve().parent.parent)
if parent_dir not in sys.path:
    sys.path.append(parent_dir)

spark = SparkSession.builder \
    .appName("Test Spark") \
    .getOrCreate()

df = spark.createDataFrame([(1, "foo"), (2, "bar")], ["id", "label"])
df.show()

spark.stop()
