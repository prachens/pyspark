from datetime import datetime

import pytest
import pandas as pd
import numpy as np
from pyspark.sql import SparkSession
# from jobs import flights_job
# from jobs import flights_job
import sys
import os
from pathlib import Path
sys.path.append("/pyspark")
# sys.path.append(Path(__file__).parent)
print(os.getcwd())
print('alamakota')
from pyspark_mp import aggregate


class TestAggregates:
    """
    This class is used to test generating of the aggregates.
    """
# UTWORZENIE SESJI PYSPARKA #

    @pytest.fixture
    def spark_session(self):

        return (     SparkSession.builder.appName('NAZWA_APLIKACJI').getOrCreate()
        )

# UTWORZENIE TABELI PYSPARKOWEJ DO SPRAWDZENIA DZIAŁANIA FUNKCJI AGREGUJĄCEJ #

    @pytest.fixture
    def bookings_test_data(self, spark_session):

        return (
            spark_session.createDataFrame(
                [
                    (
                        '000004', datetime.strptime('2017-06-13 08:25:00', '%Y-%m-%d %H:%M:%S'), 55800.00
                    ),
                    (
                        '00000F', datetime.strptime('2017-06-13 08:20:00', '%Y-%m-%d %H:%M:%S'), 265700.00
                    ),
                    (
                        '0000010', datetime.strptime('2017-06-13 08:15:00', '%Y-%m-%d %H:%M:%S'), 50900.00
                    ),
                ],
                [
                    "book_ref", "book_date", "total_amount",
                ]
            )
        )

    @pytest.fixture
    def tickets_test_data(self, spark_session):

        return (
            spark_session.createDataFrame(
                [
                    (
                        '001' , '000004', 'XYZ1', 'ola', '0123'
                    ),
                    (
                        '002' , '00000F', 'XYZ2', 'ula', '456'
                    ),
                    (
                        '003' , '0000010','XYZ3', 'ala', '789'
                    ),
                ],
                [
                    "ticket_no", "book_ref", "passenger_id", "passenger_name", "contact_data"
                ]
            )
        )


    @pytest.fixture
    def ticket_flights_test_data(self, spark_session):

        return (
            spark_session.createDataFrame(
                [
                    (
                        '001' , 'fid01',  'economy', 10
                    ),
                    (
                        '002' , 'fid02', 'economy', 20
                    ),
                    (
                        '003' , 'fid03', 'economy', 30
                    ),
                ],
                [
                    "ticket_no", "flight_id", "fare_conditions", "amount"
                ]
            )
        )



    @pytest.fixture
    def flights_test_data(self, spark_session):

        return (
            spark_session.createDataFrame(
                [
                    (
                        'fid01' , 'fn01',  '2017-09-14 11:10:00 UTC',  '2017-09-14 12:10:00 UTC', 'WAW', 'WRC'
                    ),
                    (
                        'fid02' , 'fn02', '2017-09-14 12:10:00 UTC',  '2017-09-14 13:10:00 UTC', 'BER', 'STR'
                    ),
                    (
                        'fid03' , 'fn03', '2017-09-14 13:10:00 UTC',  '2017-09-14 17:10:00 UTC', 'OSL', 'BRK'
                    ),
                ],
                [
                    "flight_id", "flight_no", "scheduled_departure", "scheduled_arrival", "departure_airport", "arrival_airport"
                ]
            )
        )



    def test_aggregation(self, spark_session, bookings_test_data, tickets_test_data, ticket_flights_test_data, flights_test_data):
    
        aggregate_df = aggregate(spark_session, bookings_test_data, tickets_test_data, ticket_flights_test_data, flights_test_data)

        # pd.testing.assert_frame_equal(AGGREGATION_FUNCTION, NAZWA_test_data, check_dtype=True)
                
        print(aggregate_df.toPandas())
        
        # spark_session.stop()