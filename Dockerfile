# Używamy oficjalnego obrazu PostgreSQL jako bazowego
FROM postgres:latest

# Zmienne środowiskowe, które zostaną ustawione przy uruchamianiu kontenera
ENV POSTGRES_USER postgres
ENV POSTGRES_PASSWORD postgres
ENV POSTGRES_DB postgres

# kopiowanie skryptu sql do dockera
COPY demo-small-en-20170815.sql /docker-entrypoint-initdb.d/

# Porty, które mają być wystawione z kontenera (domyślnie 5432 to port PostgreSQL)
EXPOSE 5432