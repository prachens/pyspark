# pierwszy job pysparkowy
import pandas as pd
import numpy as np
import sys
import os
import pyspark
from pyspark.sql import SparkSession
from pyspark.sql import DataFrameReader
from pyspark.sql import SQLContext
from pyspark import SparkContext
import re
import geopandas as gpd
import matplotlib.pyplot as plt
import seaborn as sns
import warnings
from pyspark.sql.functions import col, format_string, date_format, format_number
from pyspark.sql.functions import count, sum, avg
warnings.filterwarnings('ignore')


from google.cloud import storage

# Tworzenie klienta Google Cloud Storage
client = storage.Client()

# Nazwa zasobnika GCS
bucket_name = 'dataproc-staging-europe-central2-305196457839-jazljgsi'

# Uzyskiwanie dostępu do zasobnika
bucket = client.get_bucket(bucket_name)

blob = bucket.blob('notebooks/jupyter/db_conn.py')
blob.download_to_filename('/tmp/db_conn.py')

sys.path.append('/tmp')

import db_conn

spark = SparkSession.builder \
        .appName('Airports_aggregates_per_day') \
        .config('spark.jars', 'gs://dataproc-staging-europe-west1-305196457839-tm6piscn/notebooks/jupyter/FLIGHTS/app/bigquery_connection/spark-3.1-bigquery-0.28.0-preview.jar') \
        .getOrCreate()


# Ustaw projekt BigQuery i ścieżkę do tabeli
def get_df(table_name:str):
    
    project_id = 'mentoring-372319'
    dataset_id = 'flights_database'
    table_id = table_name
    full_table_path = f"{project_id}:{dataset_id}.{table_id}"

    # Odczytaj dane z BigQuery
    df = spark.read.format('com.google.cloud.spark.bigquery.BigQueryRelationProvider').option('table', full_table_path).load()
    
    return df


df_aggregates = get_df(table_name='aggregates')
df_aircrafts_data = get_df(table_name='aircrafts_data')
df_airports_coordinates = get_df(table_name='airports_coordinates')
df_airports_data = get_df(table_name='airports_data')
df_boarding_passes = get_df(table_name='boarding_passes')
df_bookings = get_df(table_name='bookings')
df_flights = get_df(table_name='flights')
df_seats = get_df(table_name='seats')
df_ticket_flights = get_df(table_name='ticket_flights')
df_ticket_flights_v1 = get_df(table_name='ticket_flights_v1')
df_tickets = get_df(table_name='tickets')



df_aggregates.createOrReplaceTempView("aggregates")
df_aircrafts_data.createOrReplaceTempView("aircrafts_data")
df_airports_coordinates.createOrReplaceTempView("airports_coordinates")
df_airports_data.createOrReplaceTempView("airports_data")
df_boarding_passes.createOrReplaceTempView("boarding_passes")
df_bookings.createOrReplaceTempView("bookings")
df_flights.createOrReplaceTempView("flights")
df_seats.createOrReplaceTempView("seats")
df_ticket_flights.createOrReplaceTempView("ticket_flights")
df_ticket_flights_v1.createOrReplaceTempView("ticket_flights_v1")
df_tickets.createOrReplaceTempView("tickets")




df_bookings.show()


dfa = spark.sql("""

select distinct b.book_ref,
				b.book_date,
				b.total_amount,
				f.departure_airport,
				f.scheduled_departure,
				f.actual_departure 
from bookings b
	left join tickets t on b.book_ref = t.book_ref
	left join ticket_flights tf on t.ticket_no = tf.ticket_no 
	left join flights f on tf.flight_id = f.flight_id;

""")

dfa = dfa.withColumn('scheduled_departure_date', date_format(dfa['scheduled_departure'], 'yyyy-MM-dd'))
dfa = dfa.withColumn('book_date_date', date_format(dfa['book_date'], 'yyyy-MM-dd'))
# dfa.show()

df1_agg_a = (dfa.groupBy('departure_airport', 'book_date_date')
                .agg(count('book_ref').alias('book_ref_count'))
                .orderBy('book_ref_count', ascending=False))

df1_agg_a.show()




from pyspark.sql.functions import col
dfa = dfa.withColumn('delay', col('actual_departure') - col('scheduled_departure')) 

df2_agg_a = (dfa.groupBy('departure_airport', 'scheduled_departure')
                .agg(avg('delay').alias('srednie_opoznienie'))
                .orderBy('srednie_opoznienie', ascending=False))

df2_agg_b = (df2_agg_a.groupBy('departure_airport')
                .agg(avg('srednie_opoznienie').alias('srednie_opoznienie'))
                .orderBy('srednie_opoznienie', ascending=False))b

df2_agg_b.show()




df_cd = spark.sql("""

select distinct f.departure_airport,
				to_date(f.scheduled_departure) as scheduled_departure_date,
				a.aircraft_code,
				s.fare_conditions,
				f.flight_id
from flights f
	left join aircrafts_data a on f.aircraft_code = a.aircraft_code
	left join seats s on a.aircraft_code = s.aircraft_code;

""")

df3_agg_a = (df_cd.groupBy('departure_airport', 'scheduled_departure_date')
                        .agg(count('flight_id').alias('flight_cnt'))
                        .orderBy('flight_cnt', ascending=False))

df3_agg_b = (df3_agg_a.groupBy('departure_airport')
                        .agg(avg('flight_cnt').alias('flight_avg_cnt_daily'))
                        .orderBy('flight_avg_cnt_daily', ascending=False))

df3_agg_b.show()




df4_agg_a = (df_cd.groupBy('departure_airport', 'fare_conditions', 'scheduled_departure_date')
                        .agg(count('flight_id').alias('flight_cnt'))
                        .orderBy('flight_cnt', ascending=False))

df4_agg_b = (df4_agg_a.groupBy('departure_airport', 'fare_conditions')
                        .agg(avg('flight_cnt').alias('flight_avg_cnt'))
                        .orderBy('flight_avg_cnt', ascending=False))

df4_agg_b.show()




df5 = spark.sql("""

select distinct f.departure_airport,
				to_date(f.scheduled_departure) as scheduled_departure_date,
				t.passenger_id
from tickets t
	left join ticket_flights tf on t.ticket_no = tf.ticket_no
	left join flights f on tf.flight_id = f.flight_id;

""")

df5_agg_a = (df5.groupBy('departure_airport', 'scheduled_departure_date')
                        .agg(count('passenger_id').alias('passenger_id_count'))
                        .orderBy('passenger_id_count', ascending=False))

df5_agg_b = (df5_agg_a.groupBy('departure_airport')
                        .agg(avg('passenger_id_count').alias('avg_passenger_id_count'))
                        .orderBy('avg_passenger_id_count', ascending=False))

df5_agg_b.show()






df6 = spark.sql("""

select distinct f.scheduled_departure,
                to_date(f.scheduled_departure) as scheduled_departure_date,
				f.departure_airport,
				f.flight_id
from flights f 

""")

df6_agg_a = (df6.groupBy('departure_airport', 'scheduled_departure_date')
                        .agg(count('flight_id').alias('flight_id_count'))
                        .orderBy('flight_id_count', ascending=False))

df6_agg_b = (df6_agg_a.groupBy('departure_airport')
                        .agg(count('flight_id_count').alias('avg_flight_id_count'))
                        .orderBy('avg_flight_id_count', ascending=False))

df6_agg_b.show()







# df6_agg_b.write.format("com.google.cloud.spark.bigquery") \
#         .option("writeMethod", "direct") \
#         .option("temporaryGcsBucket", 'dataproc-staging-europe-central2-305196457839-jazljgsi') \
#         .save('flights_database.df6_agg')